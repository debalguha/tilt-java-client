package org.tilt.api;

import java.util.Collection;

import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CommentModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.SettlementModel;

public interface CampaignApi {
	public Collection<CampaignModel> listCampaigns(int pageNumber);
	public CampaignModel createCampaign(CampaignModel campaign); //POST request
	public CampaignModel getCampaign(String campaignId);
	public CampaignModel updateCampaign(CampaignModel campaign); //PUT request
	public Collection<PaymentModel> listCampaignPayments(String campaignId, int pageNumber);
	public PaymentModel getCampaignPayment(String campaignId, String paymentId);
	public PaymentModel createCampaignPayment(String campaignId, PaymentModel payment); //POST request
	public PaymentModel updateCampaignPayment(String campaignId, PaymentModel payment); //PUT request
	public Collection<PaymentModel> getRejectedPayments(String campaignId, int pageNumber);
	public ResponseMessage refundPayment(String campaignId, String paymentId); //POST request
	public Collection<SettlementModel> listCampaignSettlements(String campaignId, int pageNumber);
	public SettlementModel getCampaignSettlement(String campaignId, String settlementid);
	public ResponseMessage updateCampaignSettlementBank(String campaignId, String settlementid, String bankId); //PUT request
	public CommentModel createCampaignComment(String campaignId, CommentModel comment); //POST request
	public Collection<CommentModel> listCampaignComments(String campaignId, int pageNumber);
	public CommentModel getCampaignComment(String campaignId, String commentId);
	public CommentModel updateCampaignComment(String campaignId, CommentModel comment); //PUT request
	public ResponseMessage deleteCampaignComment(String campaignId, String commentId); //DELETE request
}
