package org.tilt.api;

import java.util.Collection;

import org.tilt.api.model.BankModel;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CardModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.UserModel;
import org.tilt.api.model.VerificationModel;

public interface UserApi {
	public Collection<UserModel> listUsers(int pageNumber);
	public UserModel findUser(String userId);
	public ResponseMessage verifyUser(VerificationModel verificationModel, String userId); //POST request
	public Collection<CampaignModel> listUserCampaigns(String userId, int pageNumber);
	public UserModel createUser(UserModel user); //POST request
	public UserModel updateUser(UserModel user); //PUT request
	public CardModel createUserCard(CardModel card, String userId); //POST request
	public CardModel updateUserCard(CardModel card, String userId); //PUT request
	public CardModel getUserCard(String userId, String cardId);
	public Collection<CardModel> listUserCards(String userId, int pageNumber);
	public ResponseMessage deleteUserCard(String userId, String cardId); //DELETE request
	public BankModel createUserBank(BankModel bank, String userId); //POST request
	public BankModel updateUserBank(BankModel bank, String userId); //PUT request
	public ResponseMessage deleteUserBank(String userId, String bankId); //DELETE request
	public Collection<BankModel> listUserbanks(String userId, int pageNumber);
	public BankModel makeUserbankDefault(String userId, BankModel bankModel); //POST request
	public BankModel getDefaultUserBank(String userId);
	public BankModel getUserBank(String userId, String bankId);
	public Collection<PaymentModel> listUserPayments(String userId, int pageNumber);
}
