package org.tilt.api.exceptions;

import java.text.MessageFormat;

public class BuilderConstraintViolationException extends TiltException{
	private static final String MSG = "There was a problem while building service. Invalid {0} provided.";
	private static final long serialVersionUID = 1L;
	public enum ConstraintType{
		API_KEY("API key"), API_SECRET("API secret"), API_ENDPOINT("API endpoint");
		private String constraint;
		ConstraintType(String constraint){
			this.constraint = constraint;
		}
		public String getConstraint() {
			return constraint;
		}
	}
	public BuilderConstraintViolationException(ConstraintType constraintType) {
		super(MessageFormat.format(MSG, constraintType.getConstraint()));
	}

}
