package org.tilt.api.exceptions;

/**
 * @author: Debal Guha
 */
public class TiltConnectionException extends TiltException {
	private static final long serialVersionUID = 1L;
	private static final String MSG = "There was a problem while creating a connection to the remote service.";

	public TiltConnectionException(Exception e) {
		super(MSG, e);
	}
}
