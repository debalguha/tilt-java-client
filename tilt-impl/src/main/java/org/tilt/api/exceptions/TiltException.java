package org.tilt.api.exceptions;

/**
 * Default Tilt exception. 
 * Represents a problem in the OAuth signing process
 * 
 * @author Debal Guha
 */

public class TiltException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 * 
	 * @param message
	 *            message explaining what went wrong
	 * @param e
	 *            original exception
	 */
	public TiltException(String message, Exception e) {
		super(message, e);
	}

	/**
	 * No-exception constructor. Used when there is no original exception
	 * 
	 * @param message
	 *            message explaining what went wrong
	 */
	public TiltException(String message) {
		super(message, null);
	}
}
