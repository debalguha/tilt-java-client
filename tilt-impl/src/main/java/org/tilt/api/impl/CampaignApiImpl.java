package org.tilt.api.impl;

import java.text.MessageFormat;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tilt.api.CampaignApi;
import org.tilt.api.exceptions.TiltException;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CommentModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.SettlementModel;
import org.tilt.api.req.endpoint.Endpoint;
import org.tilt.api.req.util.TiltJacksonMapper;
import org.tilt.api.req.util.Request;
import org.tilt.api.req.util.Response;
import org.tilt.api.resp.adapter.AdapterRegistry;
import org.tilt.service.TiltService;

import com.fasterxml.jackson.databind.node.ObjectNode;

@SuppressWarnings("unchecked")
public class CampaignApiImpl implements CampaignApi{
	private static final Logger logger = LoggerFactory.getLogger(UserApiImpl.class);
	private final TiltService service;
	
	public CampaignApiImpl(TiltService service){
		this.service = service;
	}
	@Override
	public Collection<CampaignModel> listCampaigns(int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListCampaignsEndPoint();
		Request request = new Request(endpoint.getVerb(), endpoint.getUrl());
		Response resp = service.fireRequestWithPayload(request, pageNumber, null, "campaigns");
		try {
			return (Collection<CampaignModel>)AdapterRegistry.getInstance().getAdapterForClass(CampaignModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public CampaignModel createCampaign(CampaignModel campaign) {
		Endpoint endpoint = service.getEndpointRegistry().getCreateCampaignEndPoint();
		Request request = new Request(endpoint.getVerb(), endpoint.getUrl());
		Response resp = service.fireRequestWithPayload(request, campaign, "campaign");
		try {
			return (CampaignModel)AdapterRegistry.getInstance().getAdapterForClass(CampaignModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public CampaignModel getCampaign(String campaignId) {
		Endpoint endpoint = service.getEndpointRegistry().getCampaignEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, null, "campaign");
		try {
			return (CampaignModel)AdapterRegistry.getInstance().getAdapterForClass(CampaignModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public CampaignModel updateCampaign(CampaignModel campaign) {
		Endpoint endpoint = service.getEndpointRegistry().getUpdateCampaignEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaign.getId()));
		Response resp = service.fireRequestWithPayload(request, campaign, "campaign");
		try {
			return (CampaignModel)AdapterRegistry.getInstance().getAdapterForClass(CampaignModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public Collection<PaymentModel> listCampaignPayments(String campaignId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListCampaignPaymentsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null, "payments");
		try {
			return (Collection<PaymentModel>)AdapterRegistry.getInstance().getAdapterForClass(PaymentModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public PaymentModel getCampaignPayment(String campaignId, String paymentId) {
		Endpoint endpoint = service.getEndpointRegistry().getCampaignPaymentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, paymentId));
		Response resp = service.fireRequestWithPayload(request, null, "payments");
		try {
			return (PaymentModel)AdapterRegistry.getInstance().getAdapterForClass(PaymentModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public PaymentModel createCampaignPayment(String campaignId, PaymentModel payment) {
		Endpoint endpoint = service.getEndpointRegistry().getCreateCampaignPaymentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, payment, "payment");
		try {
			return (PaymentModel)AdapterRegistry.getInstance().getAdapterForClass(PaymentModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public PaymentModel updateCampaignPayment(String campaignId, PaymentModel payment) {
		Endpoint endpoint = service.getEndpointRegistry().getUpdateCampaignPaymentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, payment.getId()));
		ObjectNode node = new TiltJacksonMapper().createObjectNode();
		node.put("card_id", payment.getCardId());
		Response resp = service.fireRequestWithPayload(request, node, "payment");
		try {
			return (PaymentModel)AdapterRegistry.getInstance().getAdapterForClass(PaymentModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public Collection<PaymentModel> getRejectedPayments(String campaignId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getRejectedPaymentsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null, "payments");
		try {
			return (Collection<PaymentModel>)AdapterRegistry.getInstance().getAdapterForClass(PaymentModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public ResponseMessage refundPayment(String campaignId, String paymentId) {
		Endpoint endpoint = service.getEndpointRegistry().getPaymentRefundEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, paymentId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			if(resp.getCode() == 200)
				return ResponseMessage.OK;
			else if(resp.getCode() == 400)
				return ResponseMessage.BAD;
			else if(resp.getCode() == 449)
				return ResponseMessage.RETRY_SSN;
			return ResponseMessage.NA;
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public Collection<SettlementModel> listCampaignSettlements(String campaignId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListCampaignSettlementsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null, "payments");
		try {
			return (Collection<SettlementModel>)AdapterRegistry.getInstance().getAdapterForClass(SettlementModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public SettlementModel getCampaignSettlement(String campaignId, String settlementid) {
		Endpoint endpoint = service.getEndpointRegistry().getCampaignSettlementEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, null, "payments");
		try {
			return (SettlementModel)AdapterRegistry.getInstance().getAdapterForClass(SettlementModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public ResponseMessage updateCampaignSettlementBank(String campaignId, String settlementId, String bankId) {
		Endpoint endpoint = service.getEndpointRegistry().getCampaignSettlementBankEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, settlementId));
		ObjectNode rootNode = new TiltJacksonMapper().createObjectNode();
		rootNode.put("id", bankId);
		Response resp = service.fireRequestWithPayload(request, rootNode, "bank");
		try {
			if(resp.getCode() == 200)
				return ResponseMessage.OK;
			else if(resp.getCode() == 400)
				return ResponseMessage.BAD;
			else if(resp.getCode() == 449)
				return ResponseMessage.RETRY_SSN;
			return ResponseMessage.NA;
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public CommentModel createCampaignComment(String campaignId, CommentModel comment) {
		Endpoint endpoint = service.getEndpointRegistry().getCreateCampaignCommentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, comment, "comment");
		try {
			return (CommentModel)AdapterRegistry.getInstance().getAdapterForClass(CommentModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public Collection<CommentModel> listCampaignComments(String campaignId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListCampaignCommentsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null, "");
		try {
			return (Collection<CommentModel>)AdapterRegistry.getInstance().getAdapterForClass(CommentModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public CommentModel getCampaignComment(String campaignId, String commentId) {
		Endpoint endpoint = service.getEndpointRegistry().getCampaignCommentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, commentId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			return (CommentModel)AdapterRegistry.getInstance().getAdapterForClass(CommentModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public CommentModel updateCampaignComment(String campaignId, CommentModel comment) {
		Endpoint endpoint = service.getEndpointRegistry().getUpdateCampaignCommentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, comment.getId()));
		TiltJacksonMapper mapper = new TiltJacksonMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("score", comment.getScore());
		rootNode.put("metadata", mapper.valueToTree(comment.getMetadata()));
		Response resp = service.fireRequestWithPayload(request, rootNode, "comment");
		try {
			return (CommentModel)AdapterRegistry.getInstance().getAdapterForClass(CommentModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	@Override
	public ResponseMessage deleteCampaignComment(String campaignId, String commentId) {
		Endpoint endpoint = service.getEndpointRegistry().getDeleteCampaignCommentEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), campaignId, commentId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			if(resp.getCode() == 200)
				return ResponseMessage.OK;
			else if(resp.getCode() == 400)
				return ResponseMessage.BAD;
			else if(resp.getCode() == 449)
				return ResponseMessage.RETRY_SSN;
			return ResponseMessage.NA;
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

}
