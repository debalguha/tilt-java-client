package org.tilt.api.impl;

import java.io.IOException;
import java.util.List;

import org.tilt.api.model.PaginationModel;
import org.tilt.api.model.TiltModel;
import org.tilt.api.model.wraper.ListResponseWrapper;
import org.tilt.api.req.util.TiltJacksonMapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class TiltListJsonDeserializer<T extends TiltModel> extends JsonDeserializer<ListResponseWrapper<T>> {
	
	private Class<T> clazz;
	private String listNodeName;
	public TiltListJsonDeserializer(Class<T> clazz, String listNodeName){
		this.clazz = clazz;
		this.listNodeName = listNodeName;
	}
	
	@Override
	public ListResponseWrapper<T> deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
		ObjectMapper mapper = new TiltJacksonMapper();
		ObjectNode rootNode = jp.readValueAsTree();
		JsonNode paginationNode = rootNode.remove("pagination");
		JsonNode tiltCollectionNode = rootNode.remove(listNodeName);
		PaginationModel paginationModel = mapper.treeToValue(paginationNode, PaginationModel.class);
		JavaType javaType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
		List<T> tiltModelCollection = mapper.readValue(tiltCollectionNode.toString(), javaType);
		return new ListResponseWrapper<T>(tiltModelCollection, paginationModel);
	}

	
}
