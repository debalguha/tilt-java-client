package org.tilt.api.impl;

import java.io.IOException;

import org.tilt.api.model.TiltModel;
import org.tilt.api.model.wraper.ResponseWrapper;
import org.tilt.api.req.util.TiltJacksonMapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class TiltModelJsonDeserializer <T extends TiltModel> extends JsonDeserializer<ResponseWrapper<T>>{

	private Class<T> clazz;
	
	public TiltModelJsonDeserializer(Class<T> clazz){
		this.clazz = clazz;
	}
	
	@Override
	public ResponseWrapper<T> deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
		ObjectMapper mapper = new TiltJacksonMapper();
		ObjectNode rootNode = jp.readValueAsTree();
		JsonNode tiltNode = rootNode.get(rootNode.fieldNames().next());
		T tiltModel = mapper.treeToValue(tiltNode, clazz);
		return new ResponseWrapper<T>(tiltModel);
	}

}
