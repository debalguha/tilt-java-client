package org.tilt.api.impl;

import java.text.MessageFormat;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tilt.api.UserApi;
import org.tilt.api.exceptions.TiltException;
import org.tilt.api.model.BankModel;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CardModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.UserModel;
import org.tilt.api.model.VerificationModel;
import org.tilt.api.req.endpoint.Endpoint;
import org.tilt.api.req.util.TiltJacksonMapper;
import org.tilt.api.req.util.Request;
import org.tilt.api.req.util.Response;
import org.tilt.api.resp.adapter.AdapterRegistry;
import org.tilt.service.TiltService;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

@SuppressWarnings("unchecked")
public class UserApiImpl implements UserApi {
	private static final Logger logger = LoggerFactory.getLogger(UserApiImpl.class);
	private final TiltService service;
	
	public UserApiImpl(TiltService service){
		this.service = service;
	}
	
	
	public Collection<UserModel> listUsers(int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListUserEndPoint();
		Request request = new Request(endpoint.getVerb(), endpoint.getUrl());
		Response resp = service.fireRequestWithPayload(request, pageNumber, null);
		try {
			return (Collection<UserModel>)AdapterRegistry.getInstance().getAdapterForClass(UserModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public UserModel findUser(String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getFindUserEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			return (UserModel)AdapterRegistry.getInstance().getAdapterForClass(UserModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public ResponseMessage verifyUser(VerificationModel verificationModel, String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getVerifyUserEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, verificationModel, "verification");
		try {
			if(resp.getCode() == 200)
				return ResponseMessage.OK;
			else if(resp.getCode() == 400)
				return ResponseMessage.BAD;
			else if(resp.getCode() == 449)
				return ResponseMessage.RETRY_SSN;
			return ResponseMessage.NA;
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public Collection<CampaignModel> listUserCampaigns(String userId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getUserCampaignsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null);
		try {
			return (Collection<CampaignModel>)AdapterRegistry.getInstance().getAdapterForClass(CampaignModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public UserModel createUser(UserModel user) {
		Endpoint endpoint = service.getEndpointRegistry().getCreateUserEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), user.getId()));
		Response resp = service.fireRequestWithPayload(request, user, "user");
		try {
			return (UserModel)AdapterRegistry.getInstance().getAdapterForClass(UserModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public UserModel updateUser(UserModel user) {
		Endpoint endpoint = service.getEndpointRegistry().getUpdateUserEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), user.getId()));
		Response resp = service.fireRequestWithPayload(request, user, "user");
		try {
			return (UserModel)AdapterRegistry.getInstance().getAdapterForClass(UserModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public CardModel createUserCard(CardModel card, String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getCreateUserCardEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, card, "card");
		try {
			return (CardModel)AdapterRegistry.getInstance().getAdapterForClass(CardModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public CardModel updateUserCard(CardModel card, String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getUpdateUserCardEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId, card.getId()));
		ObjectNode rootNode = JsonNodeFactory.instance.objectNode();
		rootNode.put("metadata", new TiltJacksonMapper().valueToTree(card.getMetadata()));
		Response resp = service.fireRequestWithPayload(request, rootNode, "card");
		try {
			return (CardModel)AdapterRegistry.getInstance().getAdapterForClass(CardModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public CardModel getUserCard(String userId, String cardId) {
		Endpoint endpoint = service.getEndpointRegistry().getUserCardEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId, cardId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			return (CardModel)AdapterRegistry.getInstance().getAdapterForClass(CardModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public ResponseMessage deleteUserCard(String userId, String cardId) {
		Endpoint endpoint = service.getEndpointRegistry().getDeleteUserCardEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId, cardId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			if(resp.getCode() == 200)
				return ResponseMessage.OK;
			else if(resp.getCode() == 400)
				return ResponseMessage.BAD;
			else if(resp.getCode() == 449)
				return ResponseMessage.RETRY_SSN;
			return ResponseMessage.NA;
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public BankModel createUserBank(BankModel bank, String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getCreateUserBankEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, bank, "bank");
		try {
			return (BankModel)AdapterRegistry.getInstance().getAdapterForClass(BankModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public BankModel updateUserBank(BankModel bank, String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getUpdateUserBankEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId, bank.getId()));
		Response resp = service.fireRequestWithPayload(request, bank, "bank");
		try {
			return (BankModel)AdapterRegistry.getInstance().getAdapterForClass(BankModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public ResponseMessage deleteUserBank(String userId, String bankId) {
		Endpoint endpoint = service.getEndpointRegistry().getDeleteUserBankEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId, bankId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			if(resp.getCode() == 200)
				return ResponseMessage.OK;
			else if(resp.getCode() == 400)
				return ResponseMessage.BAD;
			else if(resp.getCode() == 449)
				return ResponseMessage.RETRY_SSN;
			return ResponseMessage.NA;
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public Collection<BankModel> listUserbanks(String userId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListUserBanksEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null);
		try {
			return (Collection<BankModel>)AdapterRegistry.getInstance().getAdapterForClass(BankModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public BankModel getUserBank(String userId, String bankId) {
		Endpoint endpoint = service.getEndpointRegistry().getUserBankEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId, bankId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			return (BankModel)AdapterRegistry.getInstance().getAdapterForClass(BankModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public Collection<PaymentModel> listUserPayments(String userId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getListUserPaymentsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null);
		try {
			return (Collection<PaymentModel>)AdapterRegistry.getInstance().getAdapterForClass(PaymentModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public Collection<CardModel> listUserCards(String userId, int pageNumber) {
		Endpoint endpoint = service.getEndpointRegistry().getUserCardsEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, pageNumber, null);
		try {
			return (Collection<CardModel>)AdapterRegistry.getInstance().getAdapterForClass(CardModel.class).wrapListResponse(resp).getTiltModels();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public BankModel makeUserbankDefault(String userId, BankModel bankModel) {
		Endpoint endpoint = service.getEndpointRegistry().getMakeUserBankDefaultEndpoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		ObjectNode node = new TiltJacksonMapper().createObjectNode();
		node.put("id", bankModel.getId());
		Response resp = service.fireRequestWithPayload(request, node, "bank");
		try {
			return (BankModel)AdapterRegistry.getInstance().getAdapterForClass(BankModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

	public BankModel getDefaultUserBank(String userId) {
		Endpoint endpoint = service.getEndpointRegistry().getDefaultUserBankEndPoint();
		Request request = new Request(endpoint.getVerb(), MessageFormat.format(endpoint.getUrl(), userId));
		Response resp = service.fireRequestWithPayload(request, null, "");
		try {
			return (BankModel)AdapterRegistry.getInstance().getAdapterForClass(BankModel.class).wrapResponse(resp).getTiltModel();
		} catch (Exception e) {
			logger.debug("Unable to parse response=> [http status code: "+resp.getCode()+", body: "+resp.getBody()+"]");
			throw new TiltException("Unable to parse response!!", e);
		}
	}

}
