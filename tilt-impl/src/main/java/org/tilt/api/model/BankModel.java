package org.tilt.api.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class BankModel extends AbstractTiltModel{
	@JsonIgnore
	private String accountLastFourDigits;
	@JsonIgnore
	private String bankCodeLastFour;
	@NotNull
	private String name;
	@JsonIgnore
	private int isDefault;
	@JsonIgnore
	private UserModel user;
	@JsonIgnore
	private String uri;
	@JsonProperty("account_number")
	@NotNull
	private String accountNumber;
	@JsonProperty("bank_code")
	@NotNull
	private String bankCode;
	
	public BankModel() {}
	
	public BankModel(String name, String accountNumber, String bankCode) {
		super();
		this.name = name;
		this.accountNumber = accountNumber;
		this.bankCode = bankCode;
	}
	@JsonIgnore
	public String getAccountLastFourDigits() {
		return accountLastFourDigits;
	}
	@JsonProperty(value="account_number_last_four")
	public void setAccountLastFourDigits(String accountLastFourDigits) {
		this.accountLastFourDigits = accountLastFourDigits;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonIgnore
	public int getIsDefault() {
		return isDefault;
	}
	@JsonProperty(value="is_default")
	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}
	@JsonIgnore
	public UserModel getUser() {
		return user;
	}
	@JsonProperty
	public void setUser(UserModel user) {
		this.user = user;
	}
	@JsonIgnore
	public String getUri() {
		return uri;
	}
	@JsonProperty
	public void setUri(String uri) {
		this.uri = uri;
	}
	@JsonIgnore
	public String getBankCodeLastFour() {
		return bankCodeLastFour;
	}
	@JsonProperty(value="bank_code_last_four")
	public void setBankCodeLastFour(String bankCodeLastFour) {
		this.bankCodeLastFour = bankCodeLastFour;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
}
