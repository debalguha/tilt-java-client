package org.tilt.api.model;

import java.util.Date;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CampaignModel extends AbstractTiltModel{
	@NotNull
	private String title;
	@JsonProperty(value="tilt_amount")
	@NotNull
	private int tiltAmount;
	@JsonProperty(value = "min_payment_amount")
	private Integer minPaymentAmount;
	@JsonProperty(value = "fixed_payment_amount")
	private Integer fixedPaymentAmount;
	@JsonProperty(value = "expiration_date")//ISO8601 DateTime format
	@NotNull
	private Date expirationDate;
	@JsonIgnore
	private int isTilted;
	@JsonIgnore
	private int isPayed;
	@JsonIgnore
	private int isExpired;
	private String uri;
	@JsonIgnore
	private String paymentUri;
	@JsonIgnore
	private int needsBank;
	@JsonIgnore
	private String settlementsUri;
	@JsonIgnore
	private UserModel admin;
	@JsonIgnore
	private UserModel firstContributor;
	@JsonIgnore
	private UserModel tilter;
	@JsonIgnore
	private Map<String, Object> stats;
	@JsonProperty("user_id")
	private String userId;
	
	public CampaignModel() {}
	
	public CampaignModel(String title, int tiltAmount, Date expirationDate, String userId) {
		super();
		this.title = title;
		this.tiltAmount = tiltAmount;
		this.expirationDate = new Date(expirationDate.getTime());
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getMinPaymentAmount() {
		return minPaymentAmount;
	}
	public void setMinPaymentAmount(int minPaymentAmount) {
		this.minPaymentAmount = minPaymentAmount;
	}
	public Integer getFixedPaymentAmount() {
		return fixedPaymentAmount;
	}
	public void setFixedPaymentAmount(int fixedPaymentAmount) {
		this.fixedPaymentAmount = fixedPaymentAmount;
	}
	public Date getExpirationDate() {
		return new Date(this.expirationDate.getTime());
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = new Date(expirationDate.getTime());
	}
	@JsonIgnore
	public int getIsTilted() {
		return isTilted;
	}
	@JsonProperty(value = "is_tilted")
	public void setIsTilted(int isTilted) {
		this.isTilted = isTilted;
	}
	@JsonIgnore
	public int getIsPayed() {
		return isPayed;
	}
	@JsonProperty(value = "is_paid")
	public void setIsPayed(int isPayed) {
		this.isPayed = isPayed;
	}
	@JsonIgnore
	public int getIsExpired() {
		return isExpired;
	}
	@JsonProperty(value = "is_expired")//0=not expired, 1 = expired
	public void setIsExpired(int isExpired) {
		this.isExpired = isExpired;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	@JsonIgnore
	public String getPaymentUri() {
		return paymentUri;
	}
	@JsonProperty(value = "payments_uri")
	public void setPaymentUri(String paymentUri) {
		this.paymentUri = paymentUri;
	}
	@JsonIgnore
	public int getNeedsBank() {
		return needsBank;
	}
	@JsonProperty(value = "needs_bank")
	public void setNeedsBank(int needsBank) {
		this.needsBank = needsBank;
	}
	@JsonIgnore
	public String getSettlementsUri() {
		return settlementsUri;
	}
	@JsonProperty(value = "settlements_uri")
	public void setSettlementsUri(String settlementsUri) {
		this.settlementsUri = settlementsUri;
	}
	@JsonIgnore
	public UserModel getAdmin() {
		return admin;
	}
	public void setAdmin(UserModel admin) {
		this.admin = admin;
	}
	@JsonIgnore
	public UserModel getFirstContributor() {
		return firstContributor;
	}
	@JsonProperty(value = "first_contributor")
	public void setFirstContributor(UserModel firstContributor) {
		this.firstContributor = firstContributor;
	}
	@JsonIgnore
	public UserModel getTilter() {
		return tilter;
	}
	public void setTilter(UserModel tilter) {
		this.tilter = tilter;
	}
	@JsonIgnore
	public Map<String, Object> getStats() {
		return stats;
	}
	public void setStats(Map<String, Object> stats) {
		this.stats = stats;
	}
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getTiltAmount() {
		return tiltAmount;
	}
	public void setTiltAmount(int tiltAmount) {
		this.tiltAmount = tiltAmount;
	}
	
}
