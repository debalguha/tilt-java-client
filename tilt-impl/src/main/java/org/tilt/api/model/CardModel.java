package org.tilt.api.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CardModel extends AbstractTiltModel{
	@JsonIgnore
	private String lastFourDigits;
	@JsonProperty(value = "expiration_month")
	private String expirationMonth;
	@JsonProperty(value = "expiration_year")
	private int expirationYear;
	@JsonProperty(value = "security_code")
	@NotNull
	private String securityCode;
	@NotNull
	private String number;
	
	@JsonIgnore
	private String cardType;
	@JsonIgnore
	private UserModel user;
	@JsonIgnore
	private String uri;

	
	public CardModel() {}
	
	public CardModel(String expirationMonth, int expirationYear, String securityCode, String number) {
		super();
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
		this.securityCode = securityCode;
		this.number = number;
	}
	@JsonIgnore
	public String getLastFourDigits() {
		return lastFourDigits;
	}
	@JsonProperty(value = "last_four")
	public void setLastFourDigits(String lastFourDigits) {
		this.lastFourDigits = lastFourDigits;
	}
	public String getExpirationMonth() {
		return expirationMonth;
	}
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	public int getExpirationYear() {
		return expirationYear;
	}
	public void setExpirationYear(int expirationYear) {
		this.expirationYear = expirationYear;
	}
	@JsonProperty(value = "card_type")
	public String getCardType() {
		return cardType;
	}
	@JsonProperty(value = "card_type")
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	@JsonIgnore
	public UserModel getUser() {
		return user;
	}
	@JsonProperty
	public void setUser(UserModel user) {
		this.user = user;
	}
	@JsonIgnore
	public String getUri() {
		return uri;
	}
	@JsonProperty
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
