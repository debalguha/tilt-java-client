package org.tilt.api.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CommentModel extends AbstractTiltModel {
	@JsonProperty(value = "user_id")
	@NotNull
	private String userId;
	@JsonProperty(value = "campaign_id")
	private String campaignId;
	@NotNull
	private String title;
	@NotNull
	private String body;
	@NotNull
	private Integer score;
	@JsonProperty(value = "parent_id")
	private String parentId;
	
	public CommentModel() {}
	public CommentModel(String userId, String campaignId, String title, String body, int score, String parentId) {
		super();
		this.userId = userId;
		this.campaignId = campaignId;
		this.title = title;
		this.body = body;
		this.score = score;
		this.parentId = parentId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
}
