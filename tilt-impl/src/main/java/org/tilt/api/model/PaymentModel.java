package org.tilt.api.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PaymentModel extends AbstractTiltModel{
	private PaymentStatus status;
	private int amount;
	@JsonProperty(value = "user_fee_amount")
	@NotNull
	private int userFeeAmount;
	@JsonProperty(value = "admin_fee_amount")
	@NotNull
	private int adminFeeAmount;
	private String uri;
	private CampaignModel campaign;
	private CardModel card;
	private UserModel user;
	@JsonProperty(value = "user_id")
	@NotNull
	private String userId;
	@JsonProperty(value = "card_id")
	@NotNull
	private String cardId;
	
	public PaymentModel(int amount, int userFeeAmount, int adminFeeAmount, String userId, String cardId) {
		super();
		this.amount = amount;
		this.userFeeAmount = userFeeAmount;
		this.adminFeeAmount = adminFeeAmount;
		this.userId = userId;
		this.cardId = cardId;
	}

	public PaymentModel() {}
	
	public PaymentStatus getStatus() {
		return status;
	}
	public void setStatus(PaymentStatus status) {
		this.status = status;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getUserFeeAmount() {
		return userFeeAmount;
	}
	public void setUserFeeAmount(int userFeeAmount) {
		this.userFeeAmount = userFeeAmount;
	}
	public int getAdminFeeAmount() {
		return adminFeeAmount;
	}
	public void setAdminFeeAmount(int adminFeeAmount) {
		this.adminFeeAmount = adminFeeAmount;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public CampaignModel getCampaign() {
		return campaign;
	}
	public void setCampaign(CampaignModel campaign) {
		this.campaign = campaign;
	}
	public CardModel getCard() {
		return card;
	}
	public void setCard(CardModel card) {
		this.card = card;
	}
	public UserModel getUser() {
		return user;
	}
	public void setUser(UserModel user) {
		this.user = user;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
}
