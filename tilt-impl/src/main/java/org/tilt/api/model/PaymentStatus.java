package org.tilt.api.model;

public enum PaymentStatus {
	authorized,charged,refunded,rejected;
}
