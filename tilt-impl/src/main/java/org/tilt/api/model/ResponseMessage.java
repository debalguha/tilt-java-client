package org.tilt.api.model;

public enum ResponseMessage {
	OK("Ok", 200),BAD("Bad Data, or Could not Verify admin information", 400),RETRY_SSN("Retry with SSN", 449), NA("NA", -1);
	private String message;
	private int httpStatusCode;
	ResponseMessage(String message, int httpStatusCode){
		this.message = message;
		this.httpStatusCode = httpStatusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	public static ResponseMessage byHttpStatus(int status){
		switch(status){
		case 200:
			return OK;
		case 400:
			return BAD;
		case 449:
			return RETRY_SSN;
		default:
			return NA;
		}
	}
}
