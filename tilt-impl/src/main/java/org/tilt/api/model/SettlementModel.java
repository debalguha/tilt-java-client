package org.tilt.api.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class SettlementModel extends AbstractTiltModel{
	private Map<String, Object> metadata;
	private SettlementStatus status;
	@JsonProperty("admin_amount")
	private int adminAmount;
	@JsonProperty("escrow_amount")
	private int escrowAmount;
	private BankModel bank;
	private CampaignModel campaign;
	private UserModel user;
	private String uri;
	public Map<String, Object> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}
	public SettlementStatus getStatus() {
		return status;
	}
	public void setStatus(SettlementStatus status) {
		this.status = status;
	}
	public int getAdminAmount() {
		return adminAmount;
	}
	public void setAdminAmount(int adminAmount) {
		this.adminAmount = adminAmount;
	}
	public int getEscrowAmount() {
		return escrowAmount;
	}
	public void setEscrowAmount(int escrowAmount) {
		this.escrowAmount = escrowAmount;
	}
	public BankModel getBank() {
		return bank;
	}
	public void setBank(BankModel bank) {
		this.bank = bank;
	}
	public CampaignModel getCampaign() {
		return campaign;
	}
	public void setCampaign(CampaignModel campaign) {
		this.campaign = campaign;
	}
	public UserModel getUser() {
		return user;
	}
	public void setUser(UserModel user) {
		this.user = user;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
}
