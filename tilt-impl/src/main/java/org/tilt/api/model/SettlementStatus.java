package org.tilt.api.model;

public enum SettlementStatus {
	
	PENDING("pending"),REJECTED("rejected"),RESENTPENDING("re-sent pending"),CLEARED("cleared");
	private String statusText;
	SettlementStatus(String statusText){
		this.statusText = statusText;
	}
	@Override
	public String toString() {
		return statusText;
	}
	
}
