package org.tilt.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class UserModel extends AbstractTiltModel{
	private String email;
	private String firstname;
	private String lastname;
	@JsonIgnore
	private int isVerified;
	@JsonIgnore
	private String uri;
	@JsonIgnore
	private String cardsUri;
	@JsonIgnore
	private String banksUri;
	@JsonIgnore
	private String campaignsUri;
	@JsonIgnore
	private String paymentsUri;

	public UserModel() {}
	public UserModel(String email, String firstname, String lastname) {
		super();
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	@JsonIgnore
	public int getIsVerified() {
		return isVerified;
	}
	@JsonProperty(value = "is_verified")
	public void setIsVerified(int isVerified) {
		this.isVerified = isVerified;
	}
	@JsonIgnore
	public String getUri() {
		return uri;
	}
	@JsonProperty
	public void setUri(String uri) {
		this.uri = uri;
	}
	@JsonIgnore
	public String getCardsUri() {
		return cardsUri;
	}
	@JsonProperty(value = "cards_uri")
	public void setCardsUri(String cardsUri) {
		this.cardsUri = cardsUri;
	}
	@JsonIgnore
	public String getBanksUri() {
		return banksUri;
	}
	@JsonProperty(value = "banks_uri")
	public void setBanksUri(String banksUri) {
		this.banksUri = banksUri;
	}
	@JsonIgnore
	public String getCampaignsUri() {
		return campaignsUri;
	}
	@JsonProperty(value = "campaigns_uri")
	public void setCampaignsUri(String campaignsUri) {
		this.campaignsUri = campaignsUri;
	}
	@JsonIgnore
	public String getPaymentsUri() {
		return paymentsUri;
	}
	@JsonProperty(value = "payments_uri")
	public void setPaymentsUri(String paymentsUri) {
		this.paymentsUri = paymentsUri;
	}
	
	
}
	