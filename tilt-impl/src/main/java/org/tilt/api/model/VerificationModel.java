package org.tilt.api.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class VerificationModel implements TiltModel{
	private String name;
	private Date dob;
	@JsonProperty("phone_number")
	private String phone;
	@JsonProperty("street_address")
	private String streetAddress;
	@JsonProperty("postal_code")
	private String postCode;
	
	public VerificationModel() {}
	public VerificationModel(String name, Date dob, String phone, String streetAddress, String postCode) {
		super();
		this.name = name;
		this.dob = new Date(dob.getTime());
		this.phone = phone;
		this.streetAddress = streetAddress;
		this.postCode = postCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDob() {
		return new Date(this.dob.getTime());
	}
	public void setDob(Date dob) {
		this.dob = new Date(dob.getTime());
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
}
