package org.tilt.api.model.wraper;

import java.util.Collection;

import org.tilt.api.model.PaginationModel;
import org.tilt.api.model.TiltModel;

public class ListResponseWrapper<T extends TiltModel> {
	public ListResponseWrapper(Collection<T> tiltModels, PaginationModel pagination) {
		super();
		this.tiltModels = tiltModels;
		this.pagination = pagination;
	}

	private Collection<T> tiltModels;
	private PaginationModel pagination;

	public Collection<T> getTiltModels() {
		return tiltModels;
	}

	public void setTiltModels(Collection<T> tiltModels) {
		this.tiltModels = tiltModels;
	}

	public PaginationModel getPagination() {
		return pagination;
	}

	public void setPagination(PaginationModel pagination) {
		this.pagination = pagination;
	}
}
