package org.tilt.api.model.wraper;

import org.tilt.api.model.TiltModel;

public class ResponseWrapper<T extends TiltModel> {
	private T tiltModel;

	public ResponseWrapper(T tiltModel) {
		super();
		this.tiltModel = tiltModel;
	}

	public T getTiltModel() {
		return tiltModel;
	}

	public void setTiltModel(T tiltModel) {
		this.tiltModel = tiltModel;
	}
}
