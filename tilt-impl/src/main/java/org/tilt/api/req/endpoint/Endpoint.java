package org.tilt.api.req.endpoint;

import org.tilt.api.req.util.Verb;

public class Endpoint {
	private final String url;
	private final Verb verb;
	public Endpoint(String url, Verb verb) {
		super();
		this.url = url;
		this.verb = verb;
	}
	public String getUrl() {
		return url;
	}
	public Verb getVerb() {
		return verb;
	}
	@Override
	public String toString() {
		return "Endpoint [url=" + url + ", verb=" + verb + "]";
	}
	
}
