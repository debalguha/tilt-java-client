package org.tilt.api.req.endpoint;

public interface EndpointRegistry {
	public Endpoint getCreateUserEndPoint();
	public Endpoint getListUserEndPoint();
	public Endpoint getFindUserEndPoint();
	public Endpoint getVerifyUserEndPoint();
	public Endpoint getUserCampaignsEndPoint();
	public Endpoint getUserCardsEndPoint();
	public Endpoint getUpdateUserEndPoint();
	public Endpoint getCreateUserCardEndPoint();
	public Endpoint getUpdateUserCardEndPoint();
	public Endpoint getUserCardEndPoint();
	public Endpoint getDeleteUserCardEndPoint();
	public Endpoint getCreateUserBankEndPoint();
	public Endpoint getUpdateUserBankEndPoint();
	public Endpoint getDeleteUserBankEndPoint();
	public Endpoint getListUserBanksEndPoint();
	public Endpoint getUserBankEndPoint();
	public Endpoint getDefaultUserBankEndPoint();
	public Endpoint getMakeUserBankDefaultEndpoint();
	public Endpoint getListUserPaymentsEndPoint();
	public Endpoint getListCampaignsEndPoint();
	public Endpoint getCreateCampaignEndPoint();
	public Endpoint getCampaignEndPoint();
	public Endpoint getUpdateCampaignEndPoint();
	public Endpoint getListCampaignPaymentsEndPoint();
	public Endpoint getCampaignPaymentEndPoint();
	public Endpoint getCreateCampaignPaymentEndPoint();
	public Endpoint getUpdateCampaignPaymentEndPoint();
	public Endpoint getRejectedPaymentsEndPoint();
	public Endpoint getPaymentRefundEndPoint();
	public Endpoint getListCampaignSettlementsEndPoint();
	public Endpoint getCampaignSettlementBankEndPoint();
	public Endpoint getUpdateCampaignSettlementBankEndPoint();
	public Endpoint getCampaignSettlementEndPoint();
	public Endpoint getCreateCampaignCommentEndPoint();
	public Endpoint getListCampaignCommentsEndPoint();
	public Endpoint getCampaignCommentEndPoint();
	public Endpoint getUpdateCampaignCommentEndPoint();
	public Endpoint getDeleteCampaignCommentEndPoint();

}
