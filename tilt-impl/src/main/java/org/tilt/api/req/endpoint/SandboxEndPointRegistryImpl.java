package org.tilt.api.req.endpoint;

import org.tilt.api.req.util.Verb;

public class SandboxEndPointRegistryImpl implements EndpointRegistry{

	public Endpoint getCreateUserEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users", Verb.POST);
	}

	public Endpoint getListUserEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users", Verb.GET);
	}

	public Endpoint getFindUserEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}", Verb.GET);
	}

	public Endpoint getVerifyUserEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/verification", Verb.POST);
	}

	public Endpoint getUserCampaignsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/campaigns", Verb.GET);
	}

	public Endpoint getUserCardsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/cards", Verb.GET);
	}

	public Endpoint getUpdateUserEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}", Verb.PUT);
	}

	public Endpoint getCreateUserCardEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/cards", Verb.POST);
	}

	public Endpoint getUpdateUserCardEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/cards/{1}", Verb.PUT);
	}

	public Endpoint getUserCardEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/cards/{1}", Verb.GET);
	}

	public Endpoint getDeleteUserCardEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/cards/{1}", Verb.DELETE);
	}

	public Endpoint getCreateUserBankEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks", Verb.POST);
	}

	public Endpoint getUpdateUserBankEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks/{1}", Verb.PUT);
	}

	public Endpoint getDeleteUserBankEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks/{1}", Verb.DELETE);
	}

	public Endpoint getListUserBanksEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks", Verb.GET);
	}

	public Endpoint getUserBankEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks/{1}", Verb.GET);
	}

	public Endpoint getListUserPaymentsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/payments", Verb.GET);
	}

	public Endpoint getListCampaignsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns", Verb.GET);
	}

	public Endpoint getCreateCampaignEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns", Verb.POST);
	}

	public Endpoint getCampaignEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}", Verb.GET);
	}

	public Endpoint getUpdateCampaignEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}", Verb.PUT);
	}

	public Endpoint getListCampaignPaymentsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/payments", Verb.GET);
	}

	public Endpoint getCampaignPaymentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/payments/{1}", Verb.GET);
	}

	public Endpoint getCreateCampaignPaymentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/payments", Verb.POST);
	}

	public Endpoint getUpdateCampaignPaymentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/payments/{1}", Verb.PUT);
	}

	public Endpoint getRejectedPaymentsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/rejected_payments", Verb.GET);
	}

	public Endpoint getPaymentRefundEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/payments/{1}/refund", Verb.POST);
	}

	public Endpoint getListCampaignSettlementsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/settlements", Verb.GET);
	}

	public Endpoint getCampaignSettlementBankEndPoint() {
		return null;
	}

	public Endpoint getUpdateCampaignSettlementBankEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/settlements/{1}/bank", Verb.POST);
	}

	public Endpoint getCampaignSettlementEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/settlements/{1}", Verb.GET);
	}

	public Endpoint getCreateCampaignCommentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/comments", Verb.POST);
	}

	public Endpoint getListCampaignCommentsEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/comments", Verb.GET);
	}

	public Endpoint getCampaignCommentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/comments/{1}", Verb.GET);
	}

	public Endpoint getUpdateCampaignCommentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/comments", Verb.PUT);
	}

	public Endpoint getDeleteCampaignCommentEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/campaigns/{0}/comments/{1}", Verb.DELETE);
	}

	public Endpoint getMakeUserBankDefaultEndpoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks/default", Verb.POST);
	}

	public Endpoint getDefaultUserBankEndPoint() {
		return new Endpoint("https://api-sandbox.tilt.com/v1/users/{0}/banks/default", Verb.GET);
	}

}
