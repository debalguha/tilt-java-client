package org.tilt.api.req.util;

import java.io.UnsupportedEncodingException;

public abstract class RequestTuner {
	public abstract void tune(Request request) throws UnsupportedEncodingException ;
}