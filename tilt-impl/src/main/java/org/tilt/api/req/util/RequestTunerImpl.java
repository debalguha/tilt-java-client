package org.tilt.api.req.util;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

public class RequestTunerImpl extends RequestTuner{
	private final String userId;
	private final String password;
	
	public RequestTunerImpl(String userId, String password) {
		super();
		this.userId = userId;
		this.password = password;
	}

	@Override
	public void tune(Request request) throws UnsupportedEncodingException {
		request.getConnection().setRequestProperty("Authorization", "Basic "+new String(Base64.encodeBase64(userId.concat(":").concat(password).getBytes("UTF-8")), "UTF-8"));
	}

}
