package org.tilt.api.req.util;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;

public class TiltJacksonMapper extends ObjectMapper {
	/**
	 * 
	 */
	private static final long serialVersionUID = -224526654492323657L;

	public TiltJacksonMapper() {
	    super();
	    this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    this.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
	    this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	    this.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
	    this.setSerializationInclusion(Include.NON_NULL);
	    this.getSerializerProvider().setNullValueSerializer(new NullSerializer());
	    this.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
	}
}

class NullSerializer extends JsonSerializer<Object> {
	public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeString("");
	}
}