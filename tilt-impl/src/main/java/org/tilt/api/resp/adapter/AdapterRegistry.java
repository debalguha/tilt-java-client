package org.tilt.api.resp.adapter;

import java.util.Map;

import org.tilt.api.model.TiltModel;

import com.google.common.collect.Maps;

public class AdapterRegistry {
	protected Map<Class<? extends TiltModel>, ResponseAdapter<? extends TiltModel>> registry = Maps.newHashMap();
	private static AdapterRegistry instance;
	public static synchronized AdapterRegistry getInstance(){
		if(instance == null)
			instance = new AdapterRegistry(); 
		return instance;
	}
	
	/*public AdapterRegistry withAdapter(ResponseAdapter<?> adapter){
		registry.put(adapter.getModelClass(), adapter);
		return instance;
	}*/
	
	public ResponseAdapter<? extends TiltModel> getAdapterForClass(Class<? extends TiltModel> modelClass){
		return registry.get(modelClass);
	}

	public AdapterRegistry withAdapter(ResponseAdapter<? extends TiltModel> responseAdapter) {
		registry.put(responseAdapter.getModelClass(), responseAdapter);
		return instance;
	}

}
