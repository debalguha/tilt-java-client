package org.tilt.api.resp.adapter;

import org.tilt.api.model.TiltModel;
import org.tilt.api.model.wraper.ListResponseWrapper;
import org.tilt.api.model.wraper.ResponseWrapper;
import org.tilt.api.req.util.Response;

public abstract class ResponseAdapter<T extends TiltModel> {
	public abstract ListResponseWrapper<T> wrapListResponse(Response resp) throws Exception;
	public abstract ResponseWrapper<T> wrapResponse(Response resp) throws Exception;
	public abstract Class<T> getModelClass();
}
