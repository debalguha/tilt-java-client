package org.tilt.api.resp.adapter;

import org.tilt.api.impl.TiltListJsonDeserializer;
import org.tilt.api.impl.TiltModelJsonDeserializer;
import org.tilt.api.model.TiltModel;
import org.tilt.api.model.wraper.ListResponseWrapper;
import org.tilt.api.model.wraper.ResponseWrapper;
import org.tilt.api.req.util.Response;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class ResponseAdapterImpl<T extends TiltModel> extends ResponseAdapter<T>{
	
	private Class<T> clazz;
	private final ObjectMapper mapper;
	
	public ResponseAdapterImpl(ObjectMapper mapper, Class<T> clazz, String listNodeName) {
		super();
		this.mapper = mapper;
		this.clazz = clazz;
		SimpleModule tiltListModule = new SimpleModule("TiltListModule", new Version(1,0,0, null, null, null)).addDeserializer(ListResponseWrapper.class, new TiltListJsonDeserializer<T>(clazz, listNodeName));
		SimpleModule tiltModule = new SimpleModule("TiltModule", new Version(1,0,0, null, null, null)).addDeserializer(ResponseWrapper.class, new TiltModelJsonDeserializer<T>(clazz));
		mapper.registerModule(tiltListModule);
		mapper.registerModule(tiltModule);
	}

	@Override
	public ListResponseWrapper<T> wrapListResponse(Response resp) throws Exception {
		return mapper.readValue(resp.getBody(), new TypeReference<ListResponseWrapper<T>>() {});
	}

	@Override
	public ResponseWrapper<T> wrapResponse(Response resp) throws Exception {
		return mapper.readValue(resp.getBody(), new TypeReference<ResponseWrapper<T>>() {});
	}

	@Override
	public Class<T> getModelClass() {
		return clazz;
	}

}
