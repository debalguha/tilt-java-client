package org.tilt.api.validator;

import org.tilt.api.model.TiltModel;
import org.tilt.api.req.util.Verb;

public abstract class AbstractCreateRequestValidator<T extends TiltModel> extends AbstractValidator<T> {
	public Verb[] getVerb() {
		return new Verb[]{Verb.POST};
	}
}
