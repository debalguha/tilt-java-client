package org.tilt.api.validator;

import org.tilt.api.model.TiltModel;

public abstract class AbstractValidator<T extends TiltModel> implements Validator<T>{
	public void bootstrap(ValidatorFactory factory) {
		factory.registerValidator(this);
	}
}
