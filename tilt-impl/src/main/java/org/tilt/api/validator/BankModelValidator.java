package org.tilt.api.validator;

import org.apache.commons.lang.StringUtils;
import org.tilt.api.model.BankModel;

import com.sankar.injectingvalidator.Path;
import com.sankar.injectingvalidator.Result;
import com.sankar.injectingvalidator.Rule;

public class BankModelValidator extends AbstractCreateRequestValidator<BankModel>{
	@Rule("account_number_required")
	public void validateAccountNumber(@Path("accountNumber") String accountNUmber, Result result){
		if(StringUtils.isBlank(accountNUmber))
			result.fail("account_number_required", "Account number can not be blank", null);
	}
	
	@Rule("name_required")
	public void validateName(@Path("name") String name, Result result){
		if(StringUtils.isBlank(name))
			result.fail("name_required", "Name can not be blank", null);
	}	
	
	@Rule("bank_code_required")
	public void validateBankCode(@Path("bankCode") String bankCode, Result result){
		if(StringUtils.isBlank(bankCode))
			result.fail("bank_code_required", "Bank code can not be blank", null);
	}

	public Class<BankModel> getModelClass() {
		return BankModel.class;
	}

}
