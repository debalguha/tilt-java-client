package org.tilt.api.validator;

import org.tilt.api.model.CampaignModel;

public class CampaignModelValidator extends AbstractCreateRequestValidator<CampaignModel> {

	public Class<CampaignModel> getModelClass() {
		return CampaignModel.class;
	}

}
