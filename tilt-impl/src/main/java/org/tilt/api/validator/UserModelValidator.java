package org.tilt.api.validator;

import org.apache.commons.lang.StringUtils;
import org.tilt.api.model.UserModel;

import com.sankar.injectingvalidator.Path;
import com.sankar.injectingvalidator.Result;
import com.sankar.injectingvalidator.Rule;

public class UserModelValidator extends AbstractCreateRequestValidator<UserModel> {
	@Rule("email_required")
	public void validateEmail(@Path("email") String email, Result result){
		if(StringUtils.isBlank(email))
			result.fail("email_required", "Email can not be null", null);
	}

	public Class<UserModel> getModelClass() {
		return UserModel.class;
	}
}
