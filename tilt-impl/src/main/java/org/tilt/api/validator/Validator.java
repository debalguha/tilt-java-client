package org.tilt.api.validator;

import org.tilt.api.model.TiltModel;
import org.tilt.api.req.util.Verb;

public interface Validator<T extends TiltModel> {
	public Verb[] getVerb();
	public Class<T> getModelClass();
	public void bootstrap(ValidatorFactory factory);
}
