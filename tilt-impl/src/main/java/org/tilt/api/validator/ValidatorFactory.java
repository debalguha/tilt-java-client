package org.tilt.api.validator;

import java.util.Map;

import org.tilt.api.model.TiltModel;
import org.tilt.api.req.util.Verb;

import com.google.common.collect.Maps;

public class ValidatorFactory {
	private Map<Class<? extends TiltModel>, Map<Verb, Validator<? extends TiltModel>>> validatorRegistry;
	
	public void registerValidator(Validator<? extends TiltModel> validator){
		Map<Verb, Validator<? extends TiltModel>> verbValidatorMap = Maps.newHashMap();
		for(Verb verb : validator.getVerb())
			verbValidatorMap.put(verb, validator);
		validatorRegistry.put(validator.getModelClass(), verbValidatorMap);
	}
}
