package org.tilt.api.validator;

import org.apache.commons.lang.StringUtils;
import org.tilt.api.model.VerificationModel;

import com.sankar.injectingvalidator.Path;
import com.sankar.injectingvalidator.Result;
import com.sankar.injectingvalidator.Rule;

public class VerificationModelValidator extends AbstractCreateRequestValidator<VerificationModel> {

	public Class<VerificationModel> getModelClass() {
		return VerificationModel.class;
	}

	@Rule("name_required")
	public void requireName(@Path("name") String name, Result result){
		if(StringUtils.isBlank(name))
			result.fail("name_required", "Name can not be blank", null);
	}
	
	@Rule("dob_required")
	public void requireDob(@Path("dob") String dob, Result result){
		if(StringUtils.isBlank(dob))
			result.fail("name_required", "Birth date can not be blank", null);
	}
}
