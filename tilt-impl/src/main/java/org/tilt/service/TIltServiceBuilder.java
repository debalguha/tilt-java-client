package org.tilt.service;

import static org.tilt.api.exceptions.BuilderConstraintViolationException.ConstraintType.API_ENDPOINT;
import static org.tilt.api.exceptions.BuilderConstraintViolationException.ConstraintType.API_KEY;
import static org.tilt.api.exceptions.BuilderConstraintViolationException.ConstraintType.API_SECRET;

import org.apache.commons.lang.StringUtils;
import org.tilt.api.exceptions.BuilderConstraintViolationException;
import org.tilt.api.impl.CampaignApiImpl;
import org.tilt.api.impl.UserApiImpl;
import org.tilt.api.model.BankModel;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CardModel;
import org.tilt.api.model.CommentModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.SettlementModel;
import org.tilt.api.model.UserModel;
import org.tilt.api.req.endpoint.EndpointType;
import org.tilt.api.req.util.TiltJacksonMapper;
import org.tilt.api.req.util.RequestTunerImpl;
import org.tilt.api.resp.adapter.AdapterRegistry;
import org.tilt.api.resp.adapter.ResponseAdapterImpl;

public class TIltServiceBuilder {
	private String apiKey;
	private String apiSecret;
	private int recordsPerPage;
	private EndpointType endpointType;
	
	public TIltServiceBuilder withEndpointType(EndpointType endpointType){
		this.endpointType = endpointType;
		return this;
	}
	public TIltServiceBuilder withApiKey(String apiKey){
		this.apiKey = apiKey;
		return this;
	}
	public TIltServiceBuilder withApiSecret(String apiSecret){
		this.apiSecret = apiSecret;
		return this;
	}
	public TIltServiceBuilder withRecordsPerPage(int recordsPerPage){
		this.recordsPerPage = recordsPerPage;
		return this;
	}
	public TiltService build() throws BuilderConstraintViolationException{
		validate();
		AdapterRegistry.getInstance().withAdapter(new ResponseAdapterImpl<UserModel>(new TiltJacksonMapper(), UserModel.class, "users"))
		.withAdapter(new ResponseAdapterImpl<BankModel>(new TiltJacksonMapper(), BankModel.class, "banks"))
		.withAdapter(new ResponseAdapterImpl<CampaignModel>(new TiltJacksonMapper(), CampaignModel.class, "campaigns"))
		.withAdapter(new ResponseAdapterImpl<CardModel>(new TiltJacksonMapper(), CardModel.class, "cards"))
		.withAdapter(new ResponseAdapterImpl<CommentModel>(new TiltJacksonMapper(), CommentModel.class, "comments"))
		.withAdapter(new ResponseAdapterImpl<PaymentModel>(new TiltJacksonMapper(), PaymentModel.class, "payments"))
		.withAdapter(new ResponseAdapterImpl<SettlementModel>(new TiltJacksonMapper(), SettlementModel.class, "settlements"));
		TiltServiceImpl service = new TiltServiceImpl(apiKey, apiSecret, endpointType, new RequestTunerImpl(apiKey, apiSecret), this.recordsPerPage==0?50:this.recordsPerPage);
		service.setUserApi(new UserApiImpl(service));
		service.setCampaignApi(new CampaignApiImpl(service));
		return service;
	}	
	private void validate() {
		if(StringUtils.isBlank(apiKey)) throw new BuilderConstraintViolationException(API_KEY);
		if(StringUtils.isBlank(apiSecret)) throw new BuilderConstraintViolationException(API_SECRET);
		if(endpointType==null) throw new BuilderConstraintViolationException(API_ENDPOINT);
	}
}
