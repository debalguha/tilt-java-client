package org.tilt.service;

import org.tilt.api.CampaignApi;
import org.tilt.api.UserApi;
import org.tilt.api.req.endpoint.EndpointRegistry;
import org.tilt.api.req.util.Request;
import org.tilt.api.req.util.Response;

public interface TiltService {
	public UserApi getUserApi();
	public CampaignApi getCampaignApi();
	public EndpointRegistry getEndpointRegistry();
	public Response fireRequestWithPayload(Request request, Object payload, String modelName);
	public Response fireRequestWithPayload(Request request, int pageNumber, Object payload, String modelName);
}
