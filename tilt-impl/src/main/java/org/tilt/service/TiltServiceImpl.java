package org.tilt.service;

import org.apache.commons.lang.StringUtils;
import org.tilt.api.CampaignApi;
import org.tilt.api.UserApi;
import org.tilt.api.exceptions.TiltException;
import org.tilt.api.req.endpoint.EndpointRegistry;
import org.tilt.api.req.endpoint.EndpointType;
import org.tilt.api.req.endpoint.SandboxEndPointRegistryImpl;
import org.tilt.api.req.util.Request;
import org.tilt.api.req.util.RequestTuner;
import org.tilt.api.req.util.Response;
import org.tilt.api.req.util.TiltJacksonMapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class TiltServiceImpl implements TiltService{
	private ObjectMapper mapper = new TiltJacksonMapper();
	private final String apiKey;
	private final String apiSecret;
	private final EndpointType endpointType;
	private RequestTuner tuner; 
	private final int recordPerPage;
	
	private UserApi userApi;
	private CampaignApi campaignApi;
	
	public TiltServiceImpl(String apiKey, String apiSecret, EndpointType endpointType, RequestTuner tuner, int recordPerPage) {
		super();
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.endpointType = endpointType;
		this.tuner = tuner;
		this.recordPerPage = recordPerPage;
	}

	public String getApiKey() {
		return apiKey;
	}

	public String getApiSecret() {
		return apiSecret;
	}
	
	public EndpointType getEndpointType() {
		return endpointType;
	}

	public EndpointRegistry getEndpointRegistry() {
		return endpointType==EndpointType.SANDBOX? new SandboxEndPointRegistryImpl():null;
	}

	@Override
	public Response fireRequestWithPayload(Request request, int pageNumber, Object payload, String modelName) {
		if(this.recordPerPage!=50)
			request.addQuerystringParameter("per_page", String.valueOf(this.recordPerPage));
		if(pageNumber > 1)
			request.addQuerystringParameter("page", String.valueOf(pageNumber));
		if(payload==null)
			return request.send(tuner);
		try {
			JsonNode rootNode = null;
			if(StringUtils.isEmpty(modelName))
				rootNode = mapper.valueToTree(payload);
			else{
				rootNode = mapper.createObjectNode();
				((ObjectNode)rootNode).put(modelName, mapper.valueToTree(payload));
			}
			request.addPayload(rootNode.toString());
		} catch (Exception e) {
			throw new TiltException("Unable to convert TiltModel ["+payload+"] into json.", e);
		}
		return request.send(tuner);
	}
	
	public Response fireRequestWithPayload(Request request, Object payload, String modelName) {
		return fireRequestWithPayload(request, 1, payload, modelName);
	}

	public UserApi getUserApi() {
		return userApi;
	}

	void setUserApi(UserApi userApi) {
		this.userApi = userApi;
	}

	public CampaignApi getCampaignApi() {
		return campaignApi;
	}

	void setCampaignApi(CampaignApi campaignApi) {
		this.campaignApi = campaignApi;
	}

	public int getRecordPerPage() {
		return recordPerPage;
	}

}
