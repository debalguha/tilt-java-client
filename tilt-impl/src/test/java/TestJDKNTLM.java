import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;

public class TestJDKNTLM {

	private void execute() throws IOException {
		System.setProperty("proxySet", "true");
		System.setProperty("https.proxyHost", "###");
		System.setProperty("https.proxyPort", "8085");
		System.setProperty("https.proxyUser", "###");
		System.setProperty("https.proxyPassword", "###");
		System.setProperty("https.auth.ntlm.domain", "###W");
		System.setProperty("http.keepAlive", "false");
		Authenticator.setDefault(new MyAuthenticator());
		URL url = new URL("https://api-sandbox.tilt.com:443/v1/users");
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		URLConnection conn = url.openConnection();

		conn.setAllowUserInteraction(true);
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String body = null;

		do {
			body = reader.readLine();
			System.out.println(body);
		} while (body != null);

	}

	public static void main(String[] args) throws IOException {
		new TestJDKNTLM().execute();
	}

	class MyAuthenticator extends Authenticator {

		public PasswordAuthentication getPasswordAuthentication() {
			if (getRequestorType() == RequestorType.PROXY) {
				String prot = getRequestingProtocol().toLowerCase();
				String host = System.getProperty(prot + ".proxyHost", "");
				String port = System.getProperty(prot + ".proxyPort", "80");
				String user = "###";//System.getProperty(prot + ".proxyUser", "");
				String password = "#####";//System.getProperty(prot + ".proxyPassword", "");

				if (getRequestingHost().equalsIgnoreCase(host)) {
					if (Integer.parseInt(port) == getRequestingPort()) {
						// Seems to be OK.
						return new PasswordAuthentication(user, password.toCharArray());
					}
				}
			}
			return new PasswordAuthentication("user", "password".toCharArray());
		}
	}
}