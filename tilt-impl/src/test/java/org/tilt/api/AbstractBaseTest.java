package org.tilt.api;

import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.math.RandomUtils;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.tilt.api.impl.CampaignApiImpl;
import org.tilt.api.model.BankModel;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CardModel;
import org.tilt.api.model.CommentModel;
import org.tilt.api.model.PaginationModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.PaymentStatus;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.SettlementModel;
import org.tilt.api.model.UserModel;
import org.tilt.api.model.VerificationModel;
import org.tilt.api.req.endpoint.SandboxEndPointRegistryImpl;
import org.tilt.api.req.util.TiltJacksonMapper;
import org.tilt.api.req.util.Request;
import org.tilt.api.req.util.Response;
import org.tilt.service.TiltService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;

public abstract class AbstractBaseTest {
	protected static TiltService service;
	protected TiltService tiltService;
	protected final static DateFormat dobFormat = new SimpleDateFormat("yyyy-MM");
	protected static UserModel createAnUser(){
		return service.getUserApi().createUser(new UserModel("test_"+RandomUtils.nextInt()+"@gmail.com", "Test", "User"));
	}
	
	protected static ResponseMessage verifyAnUser(String userId){
		return service.getUserApi().verifyUser(new VerificationModel("Test User", new Date(), "2673-9113", "10 Elis Way", "E20 1AL"), userId);
	}
	
	protected static UserModel findAnUser(String userId){
		return service.getUserApi().findUser(userId);
	}
	
	protected Collection<CampaignModel> listUserCampaigns(String userId){
		return service.getUserApi().listUserCampaigns(userId, 1);
	}
	
	protected UserModel updateUser(UserModel user) {
		return service.getUserApi().updateUser(user);
	}
	
	protected CardModel createUserCard(CardModel cardModel, String userId) {
		return service.getUserApi().createUserCard(cardModel, userId);
	}
	
	protected CardModel updateUserCard(CardModel cardModel, String userId) {
		return service.getUserApi().updateUserCard(cardModel, userId);
	}
	
	protected Collection<CardModel> listUserCards(String userId) {
		return service.getUserApi().listUserCards(userId, 1);
	}
	
	protected CardModel findUserCard(String userId, String cardId) {
		return service.getUserApi().getUserCard(userId, cardId);
	}
	
	protected ResponseMessage deleteUserCard(String userId, String cardId) {
		return service.getUserApi().deleteUserCard(userId, cardId);
	}
	
	protected BankModel createUserBank(BankModel bankModel, String userId) {
		return service.getUserApi().createUserBank(bankModel, userId);
	}
	
	protected BankModel updateUserBank(BankModel bankModel, String userId) {
		return service.getUserApi().updateUserBank(bankModel, userId);
	}	
	
	protected ResponseMessage deleteUserBank(String userId, String bankId) {
		return service.getUserApi().deleteUserBank(userId, bankId);
	}	
	
	protected Collection<BankModel> listUserBanks(String userId) {
		return service.getUserApi().listUserbanks(userId, 1);
	}
	
	protected BankModel makeUserbankDefault(String userId, BankModel bankModel) {
		return service.getUserApi().makeUserbankDefault(userId, bankModel);
	}
	
	protected BankModel getDefaultUserBank(String id) {
		return service.getUserApi().getDefaultUserBank(id);
	}
	
	protected BankModel getUserBank(String userId, String bankId) {
		return service.getUserApi().getUserBank(userId, bankId);
	}	
	
	protected Collection<PaymentModel> listUserPayments(String userId) {
		return service.getUserApi().listUserPayments(userId, 1);
	}	
	
	protected CampaignModel createACampaign(CampaignModel model) {
		return service.getCampaignApi().createCampaign(model);
	}
	
	protected CampaignModel getACampaign(String campaignId) {
		return service.getCampaignApi().getCampaign(campaignId);
	}
	
	protected CampaignModel updateCampaign(CampaignModel model) {
		return service.getCampaignApi().updateCampaign(model);
	}
	
	protected PaymentModel createCampaignPayment(String campaignId, PaymentModel payment) {
		return service.getCampaignApi().createCampaignPayment(campaignId, payment);
	}
	
	protected Collection<PaymentModel> listCampaignPayments(String campaignId) {
		return service.getCampaignApi().listCampaignPayments(campaignId, 1);
	}
	
	protected PaymentModel getCampaignPayment(String campaignId, String paymentId) {
		return service.getCampaignApi().getCampaignPayment(campaignId, paymentId);
	}	
	
	protected PaymentModel updateCampaignPayment(String campaignId, PaymentModel paymentModel) {
		return service.getCampaignApi().updateCampaignPayment(campaignId, paymentModel);
	}
	
	protected Collection<PaymentModel> getRejectedPayments(final String campaignId) {
		tiltService = Mockito.mock(TiltService.class);
		CampaignApi mockCampaignApi = new CampaignApiImpl(tiltService);
		when(tiltService.getCampaignApi()).thenReturn(mockCampaignApi);
		when(tiltService.getEndpointRegistry()).thenReturn(new SandboxEndPointRegistryImpl());
		/*when(mockCampaignApi.getRejectedPayments(campaignId)).then(new Answer<Collection<PaymentModel>>() {
			@Override
			public Collection<PaymentModel> answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}
		});*/
		when(tiltService.fireRequestWithPayload(Mockito.any(Request.class), Mockito.anyObject(), Mockito.anyString())).then(new Answer<Response>() {
			@Override
			public Response answer(InvocationOnMock invocation) throws Throwable {
				Response mockResponse = Mockito.mock(Response.class);
				when(mockResponse.getBody()).then(new Answer<String>() {
					@Override
					public String answer(InvocationOnMock invocation) throws Throwable {
						ObjectMapper mapper = new TiltJacksonMapper();
						Collection<PaymentModel> campaignPayments = listCampaignPayments(campaignId);
						Collection<PaymentModel> rejectedPayments = Lists.newArrayList();
						PaymentModel paymentModel = campaignPayments.iterator().next();
						paymentModel.setStatus(PaymentStatus.rejected);
						rejectedPayments.add(paymentModel);
						PaginationModel paginationModel = new PaginationModel();
						paginationModel.setEntriesPerPage(50);
						paginationModel.setNumberOfEntries(1);
						paginationModel.setTotalEntries(1);
						paginationModel.setPageNum(1);
						ObjectNode root = mapper.createObjectNode();
						root.put("pagination", mapper.valueToTree(paginationModel));
						root.put("payments", mapper.valueToTree(rejectedPayments));
						 
						return mapper.writeValueAsString(root);
					}
				});
				return mockResponse;
			}
		});
		return tiltService.getCampaignApi().getRejectedPayments(campaignId, 1);
	}	
	
	protected ResponseMessage refundPayment(String campaignId, String paymentId) {
		return service.getCampaignApi().refundPayment(campaignId, paymentId);
	}	
	
	protected Collection<SettlementModel> listCampaignSettlements(String campaignId) {
		return service.getCampaignApi().listCampaignSettlements(campaignId, 1);
	}
	
	protected SettlementModel getCampaignSettlement(String campaignId, String settlementid) {
		return service.getCampaignApi().getCampaignSettlement(campaignId, settlementid);
	}	
	
	protected CommentModel createCampaignComment(String campaignId, CommentModel commentModel) {
		return service.getCampaignApi().createCampaignComment(campaignId, commentModel);
	}	
	
	protected CommentModel updateCampaignComment(String campaignId, CommentModel commentModel) {
		return service.getCampaignApi().updateCampaignComment(campaignId, commentModel);
	}
	
	protected Collection<CommentModel> listComments(String campaignId) {
		return service.getCampaignApi().listCampaignComments(campaignId, 1);
	}	
	
	protected CommentModel getComment(String campaignId, String commentId) {
		return service.getCampaignApi().getCampaignComment(campaignId, commentId);
	}	
	
	protected ResponseMessage deleteCampaignComment(String campaignId, String commentId) {
		return service.getCampaignApi().deleteCampaignComment(campaignId, commentId);
	}	
}
