package org.tilt.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.tilt.api.exceptions.TiltException;
import org.tilt.api.model.BankModel;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CardModel;
import org.tilt.api.model.CommentModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.SettlementModel;
import org.tilt.api.model.UserModel;
import org.tilt.api.req.endpoint.EndpointType;
import org.tilt.service.TIltServiceBuilder;

public class CampaignApiTest extends AbstractBaseTest {

	@BeforeClass
	public static void bootstrap() throws FileNotFoundException, IOException {
		Properties props = new Properties();
		props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
		String apiKey = props.getProperty("tilt.api.key");
		String apiSecret = props.getProperty("tilt.api.secret");
		service = new TIltServiceBuilder().withApiKey(apiKey).withApiSecret(apiSecret).withEndpointType(EndpointType.SANDBOX).build();
		MockitoAnnotations.initMocks(CampaignApiTest.class);
	}

	@Test
	@Ignore
	public void shouldBeAbleToListCampaigns() {
		try {
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			createACampaign(new CampaignModel("Test Campaign " + RandomUtils.nextInt(), RandomUtils.nextInt(), new Date(), userModel.getId()));
			createACampaign(new CampaignModel("Test Campaign " + RandomUtils.nextInt(), RandomUtils.nextInt(), new Date(), userModel.getId()));
			createACampaign(new CampaignModel("Test Campaign " + RandomUtils.nextInt(), RandomUtils.nextInt(), new Date(), userModel.getId()));
			Collection<CampaignModel> campaigns = service.getCampaignApi().listCampaigns(1);
			assertNotNull(campaigns);
			assertFalse(campaigns.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToCreateCampaigns() {
		try {
			UserModel userModel = createAnUser();
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel model = new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId());
			model = createACampaign(model);
			assertNotNull(model);
			assertTrue(model.getTitle().equals(title));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToGetCampaign() {
		try {
			UserModel userModel = createAnUser();
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel model = new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId());
			model = createACampaign(model);
			model = getACampaign(model.getId());
			assertNotNull(model);
			assertEquals(title, model.getTitle());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToUpdateCampaign() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel model = new CampaignModel(title, 2000, cal.getTime(), userModel.getId());
			model = createACampaign(model);
			model.setTitle(title.concat("_update"));
			cal.add(Calendar.DAY_OF_MONTH, -1);
			model.setExpirationDate(cal.getTime());
			model.setUri(null);
			model = updateCampaign(model);
			assertNotNull(model);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToCreateCampaignPayment() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			makeUserbankDefault(userModel.getId(), createUserBank(new BankModel("CITI Bank", "123-222-5543", "026009593"), userModel.getId()));
			UserModel paymentUser = createAnUser();
			verifyAnUser(paymentUser.getId());
			CardModel cardForPaymentUser = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), paymentUser.getId());
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, 2000, cal.getTime(), userModel.getId()));
			PaymentModel paymentModel = createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			assertNotNull(paymentModel);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToListCampaignPayments() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			UserModel paymentUser = createAnUser();
			verifyAnUser(paymentUser.getId());
			CardModel cardForPaymentUser = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), paymentUser.getId());
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, 2000, cal.getTime(), userModel.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			Collection<PaymentModel> payments = listCampaignPayments(campaignModel.getId());
			assertNotNull(payments);
			assertFalse(payments.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToGetCampaignPayment() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		UserModel userModel = createAnUser();
		verifyAnUser(userModel.getId());
		String title = "Test Campaign " + RandomUtils.nextInt();
		UserModel paymentUser = createAnUser();
		verifyAnUser(paymentUser.getId());
		CardModel cardForPaymentUser = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), paymentUser.getId());
		CampaignModel campaignModel = createACampaign(new CampaignModel(title, 2000, cal.getTime(), userModel.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		PaymentModel paymentModel = createCampaignPayment(campaignModel.getId(), new PaymentModel(300, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		paymentModel = getCampaignPayment(campaignModel.getId(), paymentModel.getId());
		assertNotNull(paymentModel);
	}

	@Test(expected = TiltException.class)
	@Ignore
	public void shouldNotBeAbleToUpdateCampaignPayment() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		UserModel userModel = createAnUser();
		verifyAnUser(userModel.getId());
		String title = "Test Campaign " + RandomUtils.nextInt();
		UserModel paymentUser = createAnUser();
		verifyAnUser(paymentUser.getId());
		CardModel cardForPaymentUser = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), paymentUser.getId());
		CampaignModel campaignModel = createACampaign(new CampaignModel(title, 2000, cal.getTime(), userModel.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		PaymentModel paymentModel = createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
		paymentModel.setStatus(null);
		paymentModel = updateCampaignPayment(campaignModel.getId(), paymentModel);
	}

	@Test
	@Ignore
	public void shouldBeAbleToGetRejectedPayments() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			UserModel paymentUser = createAnUser();
			verifyAnUser(paymentUser.getId());
			CardModel cardForPaymentUser = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), paymentUser.getId());
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, 2000, cal.getTime(), userModel.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			cal.add(Calendar.DAY_OF_MONTH, -1);
			campaignModel.setExpirationDate(cal.getTime());
			campaignModel.setUri(null);
			campaignModel = updateCampaign(campaignModel);
			Collection<PaymentModel> rejectedPayments = getRejectedPayments(campaignModel.getId());
			assertNotNull(rejectedPayments);
			assertFalse(rejectedPayments.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToRefundPayment() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			UserModel paymentUser = createAnUser();
			verifyAnUser(paymentUser.getId());
			CardModel cardForPaymentUser = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), paymentUser.getId());
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, 2000, cal.getTime(), userModel.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(200, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			PaymentModel paymentModel = createCampaignPayment(campaignModel.getId(), new PaymentModel(1000, 40, 40, paymentUser.getId(), cardForPaymentUser.getId()));
			ResponseMessage resp = refundPayment(campaignModel.getId(), paymentModel.getId());
			assertNotNull(resp);
			assertEquals(ResponseMessage.OK, resp);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToListCampaignSettlements() {
		try {
			UserModel userModel = createAnUser();
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			Collection<SettlementModel> settlements = listCampaignSettlements(campaignModel.getId());
			assertNotNull(settlements);
			assertFalse(settlements.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToGetCampaignSettlement() {
		try {
			UserModel userModel = createAnUser();
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			Collection<SettlementModel> settlements = listCampaignSettlements(campaignModel.getId());
			SettlementModel settlementModel = getCampaignSettlement(campaignModel.getId(), settlements.iterator().next().getId());
			assertNotNull(settlementModel);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToUpdateCampaignSettlementBank() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	public void shouldBeAbleToCreateCampaignComment() {
		try {
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			CommentModel commentModel = createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			assertNotNull(commentModel);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	public void shouldBeAbleToListCampaignComments() {
		try {
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			CommentModel parentCampaignCommentModel = createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment_child", "Test Body", 0, parentCampaignCommentModel.getId()));
			Collection<CommentModel> comments = listComments(campaignModel.getId());
			assertNotNull(comments);
			assertFalse(comments.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToGetCampaignComment() {
		try {
			UserModel userModel = createAnUser();
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			Collection<CommentModel> comments = listComments(campaignModel.getId());
			CommentModel comment = getComment(campaignModel.getId(), comments.iterator().next().getId());
			assertNotNull(comment);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	public void shouldBeAbleToUpdateCampaignComment() {
		try {
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			CommentModel commentModel = createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			commentModel.setBody("Updated body");
			commentModel = updateCampaignComment(campaignModel.getId(), commentModel);
			assertNotNull(commentModel);
			assertEquals("Updated body", commentModel.getBody());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

	@Test
	public void shouldBeAbleToDeleteCampaignComment() {
		try {
			UserModel userModel = createAnUser();
			verifyAnUser(userModel.getId());
			String title = "Test Campaign " + RandomUtils.nextInt();
			CampaignModel campaignModel = createACampaign(new CampaignModel(title, RandomUtils.nextInt(), new Date(), userModel.getId()));
			CommentModel commentModel = createCampaignComment(campaignModel.getId(), new CommentModel(userModel.getId(), campaignModel.getId(), "Test Comment", "Test Body", 0, null));
			ResponseMessage resp = deleteCampaignComment(campaignModel.getId(), commentModel.getId());
			assertNotNull(resp);
			assertEquals(ResponseMessage.OK, resp);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed!!");
		}
	}

}