package org.tilt.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.tilt.api.model.UserModel;
import org.tilt.api.model.wraper.ListResponseWrapper;
import org.tilt.api.model.wraper.ResponseWrapper;
import org.tilt.api.req.util.Response;
import org.tilt.api.resp.adapter.ResponseAdapter;
import org.tilt.api.resp.adapter.ResponseAdapterImpl;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseAdapterTest {

	@Mock
	private Response resp;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldBeAbleToWrapListResponse() {
		String jsonStr = "{\"pagination\":{\"page\":1,\"entries_on_this_page\":3,\"total_pages\":1,\"total_entries\":3,\"per_page\":50},\"users\":[{\"id\":\"USREC5\",\"email\":\"foo.bar@gmail.com\",\"firstname\":\"Foo\",\"lastname\":\"Bar\",\"is_verified\":0,\"creation_date\":\"2011-07-02T14:20:48Z\",\"modification_date\":\"2011-09-02T14:20:48Z\",\"uri\":\"/v1/users/USREC5\",\"cards_uri\":\"/v1/users/USREC5/cards\",\"banks_uri\":\"/v1/users/USREC5/banks\",\"campaigns_uri\":\"/v1/users/USREC5/campaigns\",\"payments_uri\":\"/v1/users/USREC5/payments\",\"metadata\":{\"img\":\"http://www.example.com/path-to-profile-image\"}}]}";
		try {
			Mockito.when(resp.getBody()).thenReturn(jsonStr);
			ResponseAdapter<UserModel> adapter = new ResponseAdapterImpl<UserModel>(new ObjectMapper(), UserModel.class, "users");
			ListResponseWrapper<UserModel> wrapper = adapter.wrapListResponse(resp);
			Assert.assertNotNull(wrapper);
			Assert.assertNotNull(wrapper.getPagination());
			Assert.assertNotNull(wrapper.getTiltModels());
			Assert.assertFalse(wrapper.getTiltModels().isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed miderably");
		}
	}

	@Test
	public void shouldBeAbleToWrapResponse() {
		String jsonStr="{\"user\":{\"firstname\":\"Test\",\"banks_uri\":\"/v1/users/USRCC23F100C5E511E4A5FF86B9F814B43C/banks\",\"email\":\"test_1551275806@gmail.com\",\"processor_id\":null,\"metadata\":{},\"id\":\"USRCC23F100C5E511E4A5FF86B9F814B43C\",\"uri\":\"/v1/users/USRCC23F100C5E511E4A5FF86B9F814B43C\",\"creation_date\":\"2015-03-08T22:52:29.106194000Z\",\"campaigns_uri\":\"/v1/users/USRCC23F100C5E511E4A5FF86B9F814B43C/campaigns\",\"lastname\":\"User\",\"payments_uri\":\"/v1/users/USRCC23F100C5E511E4A5FF86B9F814B43C/payments\",\"is_verified\":0,\"modification_date\":\"2015-03-08T22:52:29.106194000Z\",\"cards_uri\":\"/v1/users/USRCC23F100C5E511E4A5FF86B9F814B43C/cards\"},\"request_id\":\"REQ299541425855149.0689268171\"}";
		try {
			Mockito.when(resp.getBody()).thenReturn(jsonStr);
			ResponseAdapter<UserModel> adapter = new ResponseAdapterImpl<UserModel>(new ObjectMapper(), UserModel.class, "users");
			ResponseWrapper<UserModel> wrapper = adapter.wrapResponse(resp);
			Assert.assertNotNull(wrapper);
			Assert.assertNotNull(wrapper.getTiltModel());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed miderably");
		}
	}

}
