package org.tilt.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.tilt.api.model.BankModel;
import org.tilt.api.model.CampaignModel;
import org.tilt.api.model.CardModel;
import org.tilt.api.model.PaymentModel;
import org.tilt.api.model.ResponseMessage;
import org.tilt.api.model.UserModel;
import org.tilt.api.req.endpoint.EndpointType;
import org.tilt.service.TIltServiceBuilder;

import com.google.common.collect.Maps;

public class UserApiTest extends AbstractBaseTest{
	@BeforeClass
	public static void bootstrap() throws FileNotFoundException, IOException{
		Properties props = new Properties();
		props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
		String apiKey = props.getProperty("tilt.api.key");
		String apiSecret = props.getProperty("tilt.api.secret");
		service = new TIltServiceBuilder().withApiKey(apiKey).withApiSecret(apiSecret).withEndpointType(EndpointType.SANDBOX).build();
	}
	
	@Test
	public void shouldBeAbleToListUsers(){
		try {
			Collection<UserModel> users = service.getUserApi().listUsers(1);
			assertNotNull(users);
			assertFalse(users.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToFindAnUser(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			user = findAnUser(user.getId());
			assertNotNull(user);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToVerifyAnUser(){
		try {
			UserModel user = createAnUser();
			ResponseMessage resp = verifyAnUser(user.getId());
			assertNotNull(resp);
			assertEquals(ResponseMessage.OK, resp);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	@Ignore
	public void shouldBeAbleToListUserCampaigns(){
		try {
			UserModel user = createAnUser();
			assertNotNull(user);
			ResponseMessage resp = verifyAnUser(user.getId());
			Assert.assertNotNull(resp);
			assertEquals(ResponseMessage.OK, resp);
			Collection<CampaignModel> campaigns = listUserCampaigns(user.getId());
			assertNotNull(campaigns);
			assertFalse(campaigns.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}	
	
	@Test
	public void shouldBeAbleToCreateUser(){
		try {
			UserModel user = createAnUser();
			assertNotNull(user);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToUpdateUser(){
		try {
			UserModel user = createAnUser();
			assertNotNull(user);
			ResponseMessage resp = verifyAnUser(user.getId());
			Assert.assertNotNull(resp);	
			user = findAnUser(user.getId());
			user.setFirstname("Test-Update");
			user = updateUser(user);
			assertNotNull(user);
			assertEquals("Test-Update", user.getFirstname());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToCreateUserCard(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			user = findAnUser(user.getId());
			CardModel cardModel = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), user.getId());
			assertNotNull(cardModel);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	/**
	 * Only metadata can be updated.
	 */
	@Test
	public void shouldBeAbleToUpdateUserCard(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			user = findAnUser(user.getId());
			CardModel cardModel = createUserCard(new CardModel("12", 2019, "555", "4111111111111111"), user.getId());
			Map<String, Object> metadata = Maps.newHashMap();
			metadata.put("issue_bank", "CITI Bank India");
			cardModel.setMetadata(metadata);
			cardModel = updateUserCard(cardModel, user.getId());
			assertNotNull(cardModel);
			assertNotNull(cardModel.getMetadata());
			assertEquals("CITI Bank India", cardModel.getMetadata().get("issue_bank"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToListUserCard(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			user = findAnUser(user.getId());
			createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), user.getId());
			createUserCard(new CardModel("11", 2020, "911", "5555555555554444"), user.getId());
			Collection<CardModel> cards =listUserCards(user.getId());
			assertNotNull(cards);
			assertFalse(cards.isEmpty());
			assertEquals(2, cards.size());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToGetUserCard(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			user = findAnUser(user.getId());
			CardModel cardModel = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), user.getId());
			cardModel = findUserCard(user.getId(), cardModel.getId());
			assertNotNull(cardModel);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToDeleteUserCard(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			user = findAnUser(user.getId());
			CardModel cardModel = createUserCard(new CardModel("12", 2019, "483", "4111111111111111"), user.getId());
			ResponseMessage resp = deleteUserCard(user.getId(), cardModel.getId());
			assertNotNull(resp);
			assertEquals(ResponseMessage.OK, resp);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToCreateUserBank(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			BankModel bankModel = createUserBank(new BankModel("ICICI Bank", "123-444-5543", "026009593"), user.getId());
			assertNotNull(bankModel);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToUpdateUserBank(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			BankModel bankModel = createUserBank(new BankModel("CITI Bank", "123-222-5543", "026009593"), user.getId());
			Map<String, Object> metaData = Maps.newHashMap();
			metaData.put("bank_name", "DENA Bank of US");
			bankModel.setMetadata(metaData);
			bankModel = updateUserBank(bankModel, user.getId());
			assertNotNull(bankModel);
			assertEquals("DENA Bank of US", bankModel.getMetadata().get("bank_name"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToDeleteUserBank(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			BankModel bankModel = createUserBank(new BankModel("RBS Bank", "123-333-5543", "026009593"), user.getId());
			ResponseMessage resp = deleteUserBank(user.getId(), bankModel.getId());
			assertNotNull(resp);
			assertEquals(ResponseMessage.OK, resp);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToListUserBank(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-5543", "026009593"), user.getId());
			Collection<BankModel> bankModels = listUserBanks(user.getId());
			assertNotNull(bankModels);
			assertFalse(bankModels.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToMakeUserBankDefault(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-5543", "026009593"), user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-777-5543", "026009593"), user.getId());
			BankModel bankModel = createUserBank(new BankModel("HDFC Bank", "123-999-1138", "026009593"), user.getId());
			bankModel = makeUserbankDefault(user.getId(), bankModel);
			assertNotNull(bankModel);
			assertEquals("1138", bankModel.getAccountLastFourDigits());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToGetDefaultUserBank(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-5543", "026009593"), user.getId());
			BankModel bankModel = createUserBank(new BankModel("HDFC Bank", "123-777-5543", "026009593"), user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-1138", "026009593"), user.getId());
			makeUserbankDefault(user.getId(), bankModel);
			bankModel = getDefaultUserBank(user.getId());
			assertNotNull(bankModel);
			assertEquals("5543", bankModel.getAccountLastFourDigits());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void shouldBeAbleToGetUserBank(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-5543", "026009593"), user.getId());
			BankModel bankModel = createUserBank(new BankModel("HDFC Bank", "123-777-5543", "026009593"), user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-1138", "026009593"), user.getId());
			bankModel = getUserBank(user.getId(), bankModel.getId());
			assertNotNull(bankModel);
			assertEquals("5543", bankModel.getAccountLastFourDigits());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	@Ignore
	public void shouldBeAbleToListUserPayments(){
		try {
			UserModel user = createAnUser();
			verifyAnUser(user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-5543", "026009593"), user.getId());
			BankModel bankModel = createUserBank(new BankModel("HDFC Bank", "123-777-5543", "026009593"), user.getId());
			createUserBank(new BankModel("HDFC Bank", "123-999-1138", "026009593"), user.getId());
			makeUserbankDefault(user.getId(), bankModel);
			Collection<PaymentModel> payments = listUserPayments(user.getId());
			assertNotNull(payments);
			assertFalse(payments.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

}